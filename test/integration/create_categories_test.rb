require 'test_helper'

#Integration test

class CreateCategoriesTest < ActionDispatch::IntegrationTest

    def setup
        @user = User.create(username: "john", email: "john@j.com", password: "password", admin: true)
    end
    test "get new category form and create category" do
        sign_in_as(@user, "password")
        get new_category_path
        assert_template 'categories/new'
        assert_difference 'Category.count', 1 do
            post categories_path, params: {category:{name:"sports"}}
            follow_redirect!
        end
        assert_template 'categories/index'
        assert_match "sports", response.body
    end

    test "invalid category submittion results in failure" do
        sign_in_as(@user, "password")
        get new_category_path
        assert_template 'categories/new'
        assert_no_difference 'Category.count' do
            post categories_path, params: {category:{name:" "}}
        end
        assert_template 'categories/new'
        assert_select 'h2.panel-title'
        assert_select 'div.panel-body' #took from the errors partial,shows how errors partials are shown in the page
    end
end