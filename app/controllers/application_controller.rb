class ApplicationController < ActionController::Base

    helper_method :current_user, :logged_in? #methods that are going to be used in our views

    def current_user
        @current_user ||= User.find(session[:user_id]) if session[:user_id] #method to not hit the DB over and over
    end

    def logged_in?
        !!current_user #boolean not current user
    end

    def require_user
        if !logged_in?
            flash[:danger] = "You must be logged in to perform that action"
            redirect_to root_path
        end
    end
end
